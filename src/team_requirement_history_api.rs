// This file is @generated by prost-build.
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct MemberUserInfo {
    #[prost(string, tag = "1")]
    pub member_user_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub member_display_name: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub member_logouri: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Perm {
    #[prost(message, repeated, tag = "1")]
    pub update_member_list: ::prost::alloc::vec::Vec<MemberUserInfo>,
    #[prost(message, repeated, tag = "2")]
    pub remove_member_list: ::prost::alloc::vec::Vec<MemberUserInfo>,
    /// 可调整权限
    #[prost(message, repeated, tag = "3")]
    pub perm_member_list: ::prost::alloc::vec::Vec<MemberUserInfo>,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateRequirementEvent {
    #[prost(message, optional, tag = "1")]
    pub perm: ::core::option::Option<Perm>,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveRequirementEvent {}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateRequirementBasicEvent {
    #[prost(string, tag = "1")]
    pub old_requirement_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateRequirementPermEvent {
    #[prost(message, optional, tag = "1")]
    pub perm: ::core::option::Option<Perm>,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LinkIssueEvent {
    #[prost(string, tag = "1")]
    pub issue_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub issue_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UnlinkIssueEvent {
    #[prost(string, tag = "1")]
    pub issue_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub issue_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct HistoryItem {
    #[prost(string, tag = "1")]
    pub event_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub requirement_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub requirement_name: ::prost::alloc::string::String,
    #[prost(int64, tag = "4")]
    pub time_stamp: i64,
    /// 操作用户ID
    #[prost(string, tag = "5")]
    pub action_user_id: ::prost::alloc::string::String,
    #[prost(string, tag = "6")]
    pub action_display_name: ::prost::alloc::string::String,
    #[prost(string, tag = "7")]
    pub action_logo_uri: ::prost::alloc::string::String,
    #[prost(oneof = "history_item::Data", tags = "10, 11, 12, 13, 14, 15")]
    pub data: ::core::option::Option<history_item::Data>,
}
/// Nested message and enum types in `HistoryItem`.
pub mod history_item {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag = "10")]
        CreateEvent(super::CreateRequirementEvent),
        #[prost(message, tag = "11")]
        RemoveEvent(super::RemoveRequirementEvent),
        #[prost(message, tag = "12")]
        UpdateBasicEvent(super::UpdateRequirementBasicEvent),
        #[prost(message, tag = "13")]
        UpdatePermEvent(super::UpdateRequirementPermEvent),
        #[prost(message, tag = "14")]
        LinkIssueEvent(super::LinkIssueEvent),
        #[prost(message, tag = "15")]
        UnlinkIssueEvent(super::UnlinkIssueEvent),
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListHistoryRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub requirement_id: ::prost::alloc::string::String,
    #[prost(uint32, tag = "10")]
    pub offset: u32,
    #[prost(uint32, tag = "11")]
    pub limit: u32,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListHistoryResponse {
    #[prost(enumeration = "list_history_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(uint32, tag = "3")]
    pub total_count: u32,
    #[prost(message, repeated, tag = "4")]
    pub item_list: ::prost::alloc::vec::Vec<HistoryItem>,
}
/// Nested message and enum types in `ListHistoryResponse`.
pub mod list_history_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
        NoRequirement = 4,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
                Code::NoRequirement => "CODE_NO_REQUIREMENT",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                "CODE_NO_REQUIREMENT" => Some(Self::NoRequirement),
                _ => None,
            }
        }
    }
}
/// Generated client implementations.
pub mod team_requirement_history_api_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    use tonic::codegen::http::Uri;
    #[derive(Debug, Clone)]
    pub struct TeamRequirementHistoryApiClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl TeamRequirementHistoryApiClient<tonic::transport::Channel> {
        /// Attempt to create a new client by connecting to a given endpoint.
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> TeamRequirementHistoryApiClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::Error: Into<StdError>,
        T::ResponseBody: Body<Data = Bytes> + Send + 'static,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_origin(inner: T, origin: Uri) -> Self {
            let inner = tonic::client::Grpc::with_origin(inner, origin);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> TeamRequirementHistoryApiClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T::ResponseBody: Default,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
            >>::Error: Into<StdError> + Send + Sync,
        {
            TeamRequirementHistoryApiClient::new(
                InterceptedService::new(inner, interceptor),
            )
        }
        /// Compress requests with the given encoding.
        ///
        /// This requires the server to support it otherwise it might respond with an
        /// error.
        #[must_use]
        pub fn send_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.inner = self.inner.send_compressed(encoding);
            self
        }
        /// Enable decompressing responses.
        #[must_use]
        pub fn accept_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.inner = self.inner.accept_compressed(encoding);
            self
        }
        /// Limits the maximum size of a decoded message.
        ///
        /// Default: `4MB`
        #[must_use]
        pub fn max_decoding_message_size(mut self, limit: usize) -> Self {
            self.inner = self.inner.max_decoding_message_size(limit);
            self
        }
        /// Limits the maximum size of an encoded message.
        ///
        /// Default: `usize::MAX`
        #[must_use]
        pub fn max_encoding_message_size(mut self, limit: usize) -> Self {
            self.inner = self.inner.max_encoding_message_size(limit);
            self
        }
        pub async fn list_history(
            &mut self,
            request: impl tonic::IntoRequest<super::ListHistoryRequest>,
        ) -> std::result::Result<
            tonic::Response<super::ListHistoryResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_requirement_history_api.TeamRequirementHistoryApi/listHistory",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new(
                        "team_requirement_history_api.TeamRequirementHistoryApi",
                        "listHistory",
                    ),
                );
            self.inner.unary(req, path, codec).await
        }
    }
}

impl crate :: TypeUrl for MemberUserInfo { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.MemberUserInfo" } }

impl crate :: TypeUrl for Perm { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.Perm" } }

impl crate :: TypeUrl for CreateRequirementEvent { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.CreateRequirementEvent" } }

impl crate :: TypeUrl for RemoveRequirementEvent { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.RemoveRequirementEvent" } }

impl crate :: TypeUrl for UpdateRequirementBasicEvent { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.UpdateRequirementBasicEvent" } }

impl crate :: TypeUrl for UpdateRequirementPermEvent { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.UpdateRequirementPermEvent" } }

impl crate :: TypeUrl for LinkIssueEvent { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.LinkIssueEvent" } }

impl crate :: TypeUrl for UnlinkIssueEvent { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.UnlinkIssueEvent" } }

impl crate :: TypeUrl for HistoryItem { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.HistoryItem" } }

impl crate :: TypeUrl for ListHistoryRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.ListHistoryRequest" } }

impl crate :: TypeUrl for ListHistoryResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_requirement_history_api.ListHistoryResponse" } }
