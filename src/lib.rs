//SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

pub mod admin_auth_api;
pub mod chat_tpl_api;
pub mod config_api;
pub mod fs_api;
pub mod team_api;
pub mod team_event_api;
pub mod team_knowledge_api;
pub mod team_knowledge_cate_api;
pub mod team_knowledge_history_api;
pub mod team_member_api;
pub mod user_api;
pub mod user_event_api;
pub mod team_issue_api;
pub mod team_issue_attr_api;
pub mod team_issue_history_api;
pub mod team_issue_stage_api;
pub mod team_issue_type_api;
pub mod team_requirement_api;
pub mod team_requirement_history_api;
pub mod team_workplan_api;
pub mod team_workplan_link_api;
pub mod team_workplan_history_api;

#[path = "."]
pub mod google {
    #[path = "google.protobuf.rs"]
    pub mod protobuf;
}

#[path = "notices.notices_team.rs"]
pub mod notices_team;

#[path = "notices.notices_knowledge.rs"]
pub mod notices_knowledge;

#[path = "notices.notices_issue.rs"]
pub mod notices_issue;

#[path = "notices.notices_requirement.rs"]
pub mod notices_requirement;

#[path = "notices.notices_workplan.rs"]
pub mod notices_workplan;

pub trait TypeUrl {
    fn type_url() -> &'static str;
}
