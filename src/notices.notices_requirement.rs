// This file is @generated by prost-build.
/// 创建需求
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct CreateRequirementNotice {
    #[prost(string, tag = "1")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub requirement_id: ::prost::alloc::string::String,
}
/// 更新需求
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateRequirementNotice {
    #[prost(string, tag = "1")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub requirement_id: ::prost::alloc::string::String,
}
/// 删除需求
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveRequirementNotice {
    #[prost(string, tag = "1")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub requirement_id: ::prost::alloc::string::String,
}

impl crate :: TypeUrl for CreateRequirementNotice { fn type_url () -> & 'static str { "type.googleapis.com/notices.notices_requirement.CreateRequirementNotice" } }

impl crate :: TypeUrl for UpdateRequirementNotice { fn type_url () -> & 'static str { "type.googleapis.com/notices.notices_requirement.UpdateRequirementNotice" } }

impl crate :: TypeUrl for RemoveRequirementNotice { fn type_url () -> & 'static str { "type.googleapis.com/notices.notices_requirement.RemoveRequirementNotice" } }
