// This file is @generated by prost-build.
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserPerm {
    #[prost(bool, tag = "1")]
    pub can_update: bool,
    #[prost(bool, tag = "2")]
    pub can_remove: bool,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct IssueAttrInfo {
    #[prost(string, tag = "1")]
    pub attr_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub attr_name: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub type_id: ::prost::alloc::string::String,
    #[prost(string, tag = "4")]
    pub type_name: ::prost::alloc::string::String,
    #[prost(uint32, tag = "5")]
    pub issue_count: u32,
    #[prost(int64, tag = "10")]
    pub create_time: i64,
    #[prost(int64, tag = "11")]
    pub update_time: i64,
    #[prost(message, optional, tag = "50")]
    pub user_perm: ::core::option::Option<UserPerm>,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddAttrRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub type_id: ::prost::alloc::string::String,
    #[prost(string, tag = "4")]
    pub attr_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddAttrResponse {
    #[prost(enumeration = "add_attr_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub attr_id: ::prost::alloc::string::String,
}
/// Nested message and enum types in `AddAttrResponse`.
pub mod add_attr_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
        NoType = 4,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
                Code::NoType => "CODE_NO_TYPE",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                "CODE_NO_TYPE" => Some(Self::NoType),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateAttrRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub attr_id: ::prost::alloc::string::String,
    #[prost(string, tag = "4")]
    pub attr_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateAttrResponse {
    #[prost(enumeration = "update_attr_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
}
/// Nested message and enum types in `UpdateAttrResponse`.
pub mod update_attr_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
        NoAttr = 4,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
                Code::NoAttr => "CODE_NO_ATTR",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                "CODE_NO_ATTR" => Some(Self::NoAttr),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListAttrRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(bool, tag = "3")]
    pub filter_by_type_id: bool,
    #[prost(string, tag = "4")]
    pub type_id: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListAttrResponse {
    #[prost(enumeration = "list_attr_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(message, repeated, tag = "3")]
    pub attr_list: ::prost::alloc::vec::Vec<IssueAttrInfo>,
}
/// Nested message and enum types in `ListAttrResponse`.
pub mod list_attr_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetAttrRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub attr_id: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetAttrResponse {
    #[prost(enumeration = "get_attr_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub attr: ::core::option::Option<IssueAttrInfo>,
}
/// Nested message and enum types in `GetAttrResponse`.
pub mod get_attr_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
        NoAttr = 4,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
                Code::NoAttr => "CODE_NO_ATTR",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                "CODE_NO_ATTR" => Some(Self::NoAttr),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveAttrRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub attr_id: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveAttrResponse {
    #[prost(enumeration = "remove_attr_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
}
/// Nested message and enum types in `RemoveAttrResponse`.
pub mod remove_attr_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                _ => None,
            }
        }
    }
}
/// Generated client implementations.
pub mod team_issue_attr_api_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    use tonic::codegen::http::Uri;
    #[derive(Debug, Clone)]
    pub struct TeamIssueAttrApiClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl TeamIssueAttrApiClient<tonic::transport::Channel> {
        /// Attempt to create a new client by connecting to a given endpoint.
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> TeamIssueAttrApiClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::Error: Into<StdError>,
        T::ResponseBody: Body<Data = Bytes> + Send + 'static,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_origin(inner: T, origin: Uri) -> Self {
            let inner = tonic::client::Grpc::with_origin(inner, origin);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> TeamIssueAttrApiClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T::ResponseBody: Default,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
            >>::Error: Into<StdError> + Send + Sync,
        {
            TeamIssueAttrApiClient::new(InterceptedService::new(inner, interceptor))
        }
        /// Compress requests with the given encoding.
        ///
        /// This requires the server to support it otherwise it might respond with an
        /// error.
        #[must_use]
        pub fn send_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.inner = self.inner.send_compressed(encoding);
            self
        }
        /// Enable decompressing responses.
        #[must_use]
        pub fn accept_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.inner = self.inner.accept_compressed(encoding);
            self
        }
        /// Limits the maximum size of a decoded message.
        ///
        /// Default: `4MB`
        #[must_use]
        pub fn max_decoding_message_size(mut self, limit: usize) -> Self {
            self.inner = self.inner.max_decoding_message_size(limit);
            self
        }
        /// Limits the maximum size of an encoded message.
        ///
        /// Default: `usize::MAX`
        #[must_use]
        pub fn max_encoding_message_size(mut self, limit: usize) -> Self {
            self.inner = self.inner.max_encoding_message_size(limit);
            self
        }
        /// 增加属性
        pub async fn add_attr(
            &mut self,
            request: impl tonic::IntoRequest<super::AddAttrRequest>,
        ) -> std::result::Result<
            tonic::Response<super::AddAttrResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_attr_api.TeamIssueAttrApi/addAttr",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new("team_issue_attr_api.TeamIssueAttrApi", "addAttr"),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 更新属性
        pub async fn update_attr(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateAttrRequest>,
        ) -> std::result::Result<
            tonic::Response<super::UpdateAttrResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_attr_api.TeamIssueAttrApi/updateAttr",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new("team_issue_attr_api.TeamIssueAttrApi", "updateAttr"),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 列出属性
        pub async fn list_attr(
            &mut self,
            request: impl tonic::IntoRequest<super::ListAttrRequest>,
        ) -> std::result::Result<
            tonic::Response<super::ListAttrResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_attr_api.TeamIssueAttrApi/listAttr",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new("team_issue_attr_api.TeamIssueAttrApi", "listAttr"),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 获取单个属性
        pub async fn get_attr(
            &mut self,
            request: impl tonic::IntoRequest<super::GetAttrRequest>,
        ) -> std::result::Result<
            tonic::Response<super::GetAttrResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_attr_api.TeamIssueAttrApi/getAttr",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new("team_issue_attr_api.TeamIssueAttrApi", "getAttr"),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 删除属性
        pub async fn remove_attr(
            &mut self,
            request: impl tonic::IntoRequest<super::RemoveAttrRequest>,
        ) -> std::result::Result<
            tonic::Response<super::RemoveAttrResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_attr_api.TeamIssueAttrApi/removeAttr",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new("team_issue_attr_api.TeamIssueAttrApi", "removeAttr"),
                );
            self.inner.unary(req, path, codec).await
        }
    }
}

impl crate :: TypeUrl for UserPerm { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.UserPerm" } }

impl crate :: TypeUrl for IssueAttrInfo { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.IssueAttrInfo" } }

impl crate :: TypeUrl for AddAttrRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.AddAttrRequest" } }

impl crate :: TypeUrl for AddAttrResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.AddAttrResponse" } }

impl crate :: TypeUrl for UpdateAttrRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.UpdateAttrRequest" } }

impl crate :: TypeUrl for UpdateAttrResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.UpdateAttrResponse" } }

impl crate :: TypeUrl for ListAttrRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.ListAttrRequest" } }

impl crate :: TypeUrl for ListAttrResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.ListAttrResponse" } }

impl crate :: TypeUrl for GetAttrRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.GetAttrRequest" } }

impl crate :: TypeUrl for GetAttrResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.GetAttrResponse" } }

impl crate :: TypeUrl for RemoveAttrRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.RemoveAttrRequest" } }

impl crate :: TypeUrl for RemoveAttrResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_attr_api.RemoveAttrResponse" } }
