// This file is @generated by prost-build.
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UserPerm {
    #[prost(bool, tag = "1")]
    pub can_update: bool,
    #[prost(bool, tag = "2")]
    pub can_remove: bool,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct IssueCustomStageInfo {
    #[prost(string, tag = "1")]
    pub custom_stage_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub stage_name: ::prost::alloc::string::String,
    #[prost(uint32, tag = "3")]
    pub issue_count: u32,
    #[prost(int64, tag = "10")]
    pub create_time: i64,
    #[prost(int64, tag = "11")]
    pub update_time: i64,
    #[prost(message, optional, tag = "50")]
    pub user_perm: ::core::option::Option<UserPerm>,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddCustomStageRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub stage_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct AddCustomStageResponse {
    #[prost(enumeration = "add_custom_stage_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub custom_stage_id: ::prost::alloc::string::String,
}
/// Nested message and enum types in `AddCustomStageResponse`.
pub mod add_custom_stage_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateCustomStageRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub custom_stage_id: ::prost::alloc::string::String,
    #[prost(string, tag = "4")]
    pub stage_name: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct UpdateCustomStageResponse {
    #[prost(enumeration = "update_custom_stage_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
}
/// Nested message and enum types in `UpdateCustomStageResponse`.
pub mod update_custom_stage_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
        NoCustomStage = 4,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
                Code::NoCustomStage => "CODE_NO_CUSTOM_STAGE",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                "CODE_NO_CUSTOM_STAGE" => Some(Self::NoCustomStage),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomStageRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListCustomStageResponse {
    #[prost(enumeration = "list_custom_stage_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(message, repeated, tag = "3")]
    pub custom_stage_list: ::prost::alloc::vec::Vec<IssueCustomStageInfo>,
}
/// Nested message and enum types in `ListCustomStageResponse`.
pub mod list_custom_stage_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetCustomStageRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub custom_stage_id: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GetCustomStageResponse {
    #[prost(enumeration = "get_custom_stage_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
    #[prost(message, optional, tag = "3")]
    pub custom_stage: ::core::option::Option<IssueCustomStageInfo>,
}
/// Nested message and enum types in `GetCustomStageResponse`.
pub mod get_custom_stage_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
        NoCustomStage = 4,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
                Code::NoCustomStage => "CODE_NO_CUSTOM_STAGE",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                "CODE_NO_CUSTOM_STAGE" => Some(Self::NoCustomStage),
                _ => None,
            }
        }
    }
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveCustomStageRequest {
    #[prost(string, tag = "1")]
    pub session_id: ::prost::alloc::string::String,
    #[prost(string, tag = "2")]
    pub team_id: ::prost::alloc::string::String,
    #[prost(string, tag = "3")]
    pub custom_stage_id: ::prost::alloc::string::String,
}
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct RemoveCustomStageResponse {
    #[prost(enumeration = "remove_custom_stage_response::Code", tag = "1")]
    pub code: i32,
    #[prost(string, tag = "2")]
    pub err_msg: ::prost::alloc::string::String,
}
/// Nested message and enum types in `RemoveCustomStageResponse`.
pub mod remove_custom_stage_response {
    #[derive(serde::Serialize, serde::Deserialize)]
    #[derive(
        Clone,
        Copy,
        Debug,
        PartialEq,
        Eq,
        Hash,
        PartialOrd,
        Ord,
        ::prost::Enumeration
    )]
    #[repr(i32)]
    pub enum Code {
        Ok = 0,
        WrongSession = 1,
        NoTeam = 2,
        NoPermission = 3,
    }
    impl Code {
        /// String value of the enum field names used in the ProtoBuf definition.
        ///
        /// The values are not transformed in any way and thus are considered stable
        /// (if the ProtoBuf definition does not change) and safe for programmatic use.
        pub fn as_str_name(&self) -> &'static str {
            match self {
                Code::Ok => "CODE_OK",
                Code::WrongSession => "CODE_WRONG_SESSION",
                Code::NoTeam => "CODE_NO_TEAM",
                Code::NoPermission => "CODE_NO_PERMISSION",
            }
        }
        /// Creates an enum from field names used in the ProtoBuf definition.
        pub fn from_str_name(value: &str) -> ::core::option::Option<Self> {
            match value {
                "CODE_OK" => Some(Self::Ok),
                "CODE_WRONG_SESSION" => Some(Self::WrongSession),
                "CODE_NO_TEAM" => Some(Self::NoTeam),
                "CODE_NO_PERMISSION" => Some(Self::NoPermission),
                _ => None,
            }
        }
    }
}
/// Generated client implementations.
pub mod team_issue_stage_api_client {
    #![allow(unused_variables, dead_code, missing_docs, clippy::let_unit_value)]
    use tonic::codegen::*;
    use tonic::codegen::http::Uri;
    #[derive(Debug, Clone)]
    pub struct TeamIssueStageApiClient<T> {
        inner: tonic::client::Grpc<T>,
    }
    impl TeamIssueStageApiClient<tonic::transport::Channel> {
        /// Attempt to create a new client by connecting to a given endpoint.
        pub async fn connect<D>(dst: D) -> Result<Self, tonic::transport::Error>
        where
            D: TryInto<tonic::transport::Endpoint>,
            D::Error: Into<StdError>,
        {
            let conn = tonic::transport::Endpoint::new(dst)?.connect().await?;
            Ok(Self::new(conn))
        }
    }
    impl<T> TeamIssueStageApiClient<T>
    where
        T: tonic::client::GrpcService<tonic::body::BoxBody>,
        T::Error: Into<StdError>,
        T::ResponseBody: Body<Data = Bytes> + Send + 'static,
        <T::ResponseBody as Body>::Error: Into<StdError> + Send,
    {
        pub fn new(inner: T) -> Self {
            let inner = tonic::client::Grpc::new(inner);
            Self { inner }
        }
        pub fn with_origin(inner: T, origin: Uri) -> Self {
            let inner = tonic::client::Grpc::with_origin(inner, origin);
            Self { inner }
        }
        pub fn with_interceptor<F>(
            inner: T,
            interceptor: F,
        ) -> TeamIssueStageApiClient<InterceptedService<T, F>>
        where
            F: tonic::service::Interceptor,
            T::ResponseBody: Default,
            T: tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
                Response = http::Response<
                    <T as tonic::client::GrpcService<tonic::body::BoxBody>>::ResponseBody,
                >,
            >,
            <T as tonic::codegen::Service<
                http::Request<tonic::body::BoxBody>,
            >>::Error: Into<StdError> + Send + Sync,
        {
            TeamIssueStageApiClient::new(InterceptedService::new(inner, interceptor))
        }
        /// Compress requests with the given encoding.
        ///
        /// This requires the server to support it otherwise it might respond with an
        /// error.
        #[must_use]
        pub fn send_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.inner = self.inner.send_compressed(encoding);
            self
        }
        /// Enable decompressing responses.
        #[must_use]
        pub fn accept_compressed(mut self, encoding: CompressionEncoding) -> Self {
            self.inner = self.inner.accept_compressed(encoding);
            self
        }
        /// Limits the maximum size of a decoded message.
        ///
        /// Default: `4MB`
        #[must_use]
        pub fn max_decoding_message_size(mut self, limit: usize) -> Self {
            self.inner = self.inner.max_decoding_message_size(limit);
            self
        }
        /// Limits the maximum size of an encoded message.
        ///
        /// Default: `usize::MAX`
        #[must_use]
        pub fn max_encoding_message_size(mut self, limit: usize) -> Self {
            self.inner = self.inner.max_encoding_message_size(limit);
            self
        }
        /// 增加自定义阶段
        pub async fn add_custom_stage(
            &mut self,
            request: impl tonic::IntoRequest<super::AddCustomStageRequest>,
        ) -> std::result::Result<
            tonic::Response<super::AddCustomStageResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_stage_api.TeamIssueStageApi/addCustomStage",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new(
                        "team_issue_stage_api.TeamIssueStageApi",
                        "addCustomStage",
                    ),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 更新自定义阶段
        pub async fn update_custom_stage(
            &mut self,
            request: impl tonic::IntoRequest<super::UpdateCustomStageRequest>,
        ) -> std::result::Result<
            tonic::Response<super::UpdateCustomStageResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_stage_api.TeamIssueStageApi/updateCustomStage",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new(
                        "team_issue_stage_api.TeamIssueStageApi",
                        "updateCustomStage",
                    ),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 列出自定义阶段
        pub async fn list_custom_stage(
            &mut self,
            request: impl tonic::IntoRequest<super::ListCustomStageRequest>,
        ) -> std::result::Result<
            tonic::Response<super::ListCustomStageResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_stage_api.TeamIssueStageApi/listCustomStage",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new(
                        "team_issue_stage_api.TeamIssueStageApi",
                        "listCustomStage",
                    ),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 获取单个自定义阶段
        pub async fn get_custom_stage(
            &mut self,
            request: impl tonic::IntoRequest<super::GetCustomStageRequest>,
        ) -> std::result::Result<
            tonic::Response<super::GetCustomStageResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_stage_api.TeamIssueStageApi/getCustomStage",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new(
                        "team_issue_stage_api.TeamIssueStageApi",
                        "getCustomStage",
                    ),
                );
            self.inner.unary(req, path, codec).await
        }
        /// 删除自定义阶段
        pub async fn remove_custom_stage(
            &mut self,
            request: impl tonic::IntoRequest<super::RemoveCustomStageRequest>,
        ) -> std::result::Result<
            tonic::Response<super::RemoveCustomStageResponse>,
            tonic::Status,
        > {
            self.inner
                .ready()
                .await
                .map_err(|e| {
                    tonic::Status::new(
                        tonic::Code::Unknown,
                        format!("Service was not ready: {}", e.into()),
                    )
                })?;
            let codec = tonic::codec::ProstCodec::default();
            let path = http::uri::PathAndQuery::from_static(
                "/team_issue_stage_api.TeamIssueStageApi/removeCustomStage",
            );
            let mut req = request.into_request();
            req.extensions_mut()
                .insert(
                    GrpcMethod::new(
                        "team_issue_stage_api.TeamIssueStageApi",
                        "removeCustomStage",
                    ),
                );
            self.inner.unary(req, path, codec).await
        }
    }
}

impl crate :: TypeUrl for UserPerm { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.UserPerm" } }

impl crate :: TypeUrl for IssueCustomStageInfo { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.IssueCustomStageInfo" } }

impl crate :: TypeUrl for AddCustomStageRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.AddCustomStageRequest" } }

impl crate :: TypeUrl for AddCustomStageResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.AddCustomStageResponse" } }

impl crate :: TypeUrl for UpdateCustomStageRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.UpdateCustomStageRequest" } }

impl crate :: TypeUrl for UpdateCustomStageResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.UpdateCustomStageResponse" } }

impl crate :: TypeUrl for ListCustomStageRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.ListCustomStageRequest" } }

impl crate :: TypeUrl for ListCustomStageResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.ListCustomStageResponse" } }

impl crate :: TypeUrl for GetCustomStageRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.GetCustomStageRequest" } }

impl crate :: TypeUrl for GetCustomStageResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.GetCustomStageResponse" } }

impl crate :: TypeUrl for RemoveCustomStageRequest { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.RemoveCustomStageRequest" } }

impl crate :: TypeUrl for RemoveCustomStageResponse { fn type_url () -> & 'static str { "type.googleapis.com/team_issue_stage_api.RemoveCustomStageResponse" } }
