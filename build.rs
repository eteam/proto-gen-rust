//SPDX-FileCopyrightText: Copyright 2022-2024 深圳市同心圆网络有限公司
//SPDX-License-Identifier: GPL-3.0-only

use prost::Message;
use prost_types::FileDescriptorSet;
use quote::{format_ident, quote};
use std::fs::{self, File, OpenOptions};
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = PathBuf::from("src");
    tonic_build::configure()
        .build_server(false)
        .type_attribute(".", "#[derive(serde::Serialize,serde::Deserialize)]")
        .out_dir("src")
        .compile_well_known_types(true)
        .file_descriptor_set_path("src/fd.bin")
        .compile(
            &[
                "admin_auth_api.proto",
                "chat_tpl_api.proto",
                "user_api.proto",
                "user_event_api.proto",
                "fs_api.proto",
                "config_api.proto",
                "team_api.proto",
                "team_event_api.proto",
                "team_member_api.proto",
                "team_knowledge_api.proto",
                "team_knowledge_cate_api.proto",
                "team_knowledge_history_api.proto",
                "team_issue_api.proto",
                "team_issue_attr_api.proto",
                "team_issue_history_api.proto",
                "team_issue_stage_api.proto",
                "team_issue_type_api.proto",
                "team_requirement_api.proto",
                "team_requirement_history_api.proto",
                "team_workplan_api.proto",
                "team_workplan_link_api.proto",
                "team_workplan_history_api.proto",
                "notices/notices_team.proto",
                "notices/notices_issue.proto",
                "notices/notices_knowledge.proto",
                "notices/notices_requirement.proto",
                "notices/notices_workplan.proto"
            ],
            &["proto/src","proto/src/third_part"],
        )
        .unwrap();
    let fd_data = fs::read("src/fd.bin").unwrap();
    let fd = FileDescriptorSet::decode(&fd_data[..]).unwrap();
    generate_extras(&out_dir, &fd);
    Ok(())
}

fn generate_extras(out_dir: &Path, file_descriptor_set: &FileDescriptorSet) {
    for fd in &file_descriptor_set.file {
        let package = match fd.package {
            Some(ref pkg) => pkg,
            None => continue,
        };

        if package.starts_with("google.") {
            continue;
        }

        let gen_path = out_dir.join(format!("{}.rs", package));
        let mut gen_file = OpenOptions::new().append(true).open(gen_path).unwrap();

        for msg in &fd.message_type {
            let name = match msg.name {
                Some(ref name) => name,
                None => continue,
            };

            let type_url = format!("type.googleapis.com/{}.{}", package, name);

            gen_type_url(&mut gen_file, &type_url, name);
        }
    }
}

fn gen_type_url(gen_file: &mut File, type_url: &str, type_name: &str) {
    let type_name = format_ident!("{}", type_name);

    let tokens = quote! {
        impl crate::TypeUrl for #type_name {
            fn type_url() -> &'static str {
                #type_url
            }
        }
    };

    writeln!(gen_file).unwrap();
    writeln!(gen_file, "{}", &tokens).unwrap();
}
