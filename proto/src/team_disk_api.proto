// SPDX-FileCopyrightText: Copyright 2025-2025 深圳市同心圆网络有限公司
// SPDX-License-Identifier: GPL-3.0-only

syntax = "proto3";

package team_disk_api;

option go_package = "./team_disk_api";

message SysFolderInfo {
  string rootFolderId = 1;
  string sysFolderId = 2;

  string issueFolderId = 10;
  string knowledgeFolderId = 11;
  string requirementFolderId = 12;
  string boardFolderId = 13;
}

message BasicFolderInfo {
  string folderName = 1;
}

message Perm {
  bool readForAll = 1;
  repeated string readMemberUserIdList = 2;
  repeated string updateMemberUserIdList = 3;
  repeated string removeMemberUserIdList = 4;
  repeated string permMemberUserIdList = 5;  //可调整权限
}

message UserPerm {
  bool canRead = 1;
  bool canUpdateBasic = 2;
  bool canUpdatePerm = 3;
  bool canRemove = 4;
}

message FolderInfo {
  string folderId = 1;  
  BasicFolderInfo basicInfo = 2;
  bool system = 3;  //系统创建的目录，不能修改
  uint32 subFolderCount = 4;
  uint32 subFileCount = 5;
  Perm perm = 6;
  string parentFolderId = 7;

  string createUserId = 20;
  string createDisplayName = 21;
  string createLogoUri = 22;
  int64 createTime = 23;

  string updateUserId = 30;
  string updateDisplayName = 31;
  string updateLogoUri = 32;
  int64 updateTime = 33;

  UserPerm userPerm = 50;
}

message BasicFileInfo {
  string fileName = 1;
}

message RefByNone {}


message RefByIssue {
  string issueId = 1;
}

message RefByKnowledge {
  string knowledgeId = 1;
}

message RefByRequirement {
  string requirementId = 1;
}

message RefByBoard {
  string boardId = 1;
  string archiveId = 2;
}

message FileInfo {
  string fileId = 1;  //和fs中fileId保持一致
  BasicFileInfo basicInfo = 2;
  bool system = 3;  //系统创建的文件，不能修改和删除
  bool usePerm = 4;  //是否使用权限
  Perm perm = 5;
  string parentFolderId = 6;
  
  oneof refBy {
    RefByNone byNone = 10;
    RefByIssue byIssue = 11;
    RefByKnowledge byKnowledge = 12;
    RefByRequirement byRequirement = 13;
    RefByBoard byBoard = 14;
  }
  int64 fileSize = 20;

  string createUserId = 30;
  string createDisplayName = 31;
  string createLogoUri = 32;
  int64 createTime = 33;

  string updateUserId = 40;
  string updateDisplayName = 41;
  string updateLogoUri = 42;
  int64 updateTime = 43;

  UserPerm userPerm = 50;
}

service TeamDiskApi {
  //获取系统目录信息
  rpc getSysFolder(GetSysFolderRequest) returns (GetSysFolderResponse) {}

  //创建目录
  rpc createFolder(CreateFolderRequest) returns (CreateFolderResponse) {}
  //列出目录
  rpc listFolder(ListFolderRequest) returns (ListFolderResponse) {}
  //修改目录基本信息
  rpc updateFolderBasic(UpdateFolderBasicRequest)
      returns (UpdateFolderBasicResponse) {}
  //修改目录权限
  rpc updateFolderPerm(UpdateFolderPermRequest)
      returns (UpdateFolderPermResponse) {}
  //获取单个目录信息
  rpc getFolder(GetFolderRequest) returns (GetFolderResponse) {}
  //删除目录
  rpc removeFolder(RemoveFolderRequest) returns (RemoveFolderResponse) {}
  //移动目录
  rpc moveFolder(MoveFolderRequest) returns (MoveFolderResponse) {}

  //创建文件
  rpc createFile(CreateFileRequest) returns (CreateFileResponse) {}
  //列出文件
  rpc listFile(ListFileRequest) returns (ListFileResponse) {}
  //修改文件基本信息
  rpc updateFileBasic(UpdateFileBasicRequest)
      returns (UpdateFileBasicResponse) {}
  //修改文件权限
  rpc updateFilePerm(UpdateFilePermRequest) returns (UpdateFilePermResponse) {}
  //获取单个文件信息
  rpc getFile(GetFileRequest) returns (GetFileResponse) {}
  //删除文件
  rpc removeFile(RemoveFileRequest) returns (RemoveFileResponse) {}
  //移动文件
  rpc moveFile(MoveFileRequest) returns (MoveFileResponse) {}
}

message GetSysFolderRequest {
  string sessionId = 1;
  string teamId = 2;
}

message GetSysFolderResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
  }
  CODE code = 1;
  string errMsg = 2;
  SysFolderInfo sysFolder = 3;
}

message CreateFolderRequest {
  string sessionId = 1;
  string teamId = 2;
  string parentFolderId = 3;
  BasicFolderInfo basicInfo = 4;
}

message CreateFolderResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_PARENT_FOLDER = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  string folderId = 3;
}

message ListFolderRequest {
  string sessionId = 1;
  string teamId = 2;
  string parentFolderId = 3;
}

message ListFolderResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_PARENT_FOLDER = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated FolderInfo folderList = 3;
}

message UpdateFolderBasicRequest {
  string sessionId = 1;
  string teamId = 2;
  string folderId = 3;
  BasicFolderInfo basicInfo = 4;
}

message UpdateFolderBasicResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FOLDER = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message UpdateFolderPermRequest {
  string sessionId = 1;
  string teamId = 2;
  string folderId = 3;
  Perm perm = 4;
}

message UpdateFolderPermResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FOLDER = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message GetFolderRequest {
  string sessionId = 1;
  string teamId = 2;
  string folderId = 3;
}

message GetFolderResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FOLDER = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  FolderInfo folder = 3;
}

message RemoveFolderRequest {
  string sessionId = 1;
  string teamId = 2;
  string folderId = 3;
}

message RemoveFolderResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NOT_ALLOW = 4;  //包含子目录或子文件
  }
  CODE code = 1;
  string errMsg = 2;
}

message MoveFolderRequest {
  string sessionId = 1;
  string teamId = 2;
  string folderId = 3;
  string parentFolderId = 4;
}

message MoveFolderResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FOLDER = 4;
    CODE_NO_PARENT_FOLDER = 5;
    CODE_NOT_ALLOW = 6;  //不能移动到自己的子目录
  }
  CODE code = 1;
  string errMsg = 2;
}

message CreateFileRequest {
  string sessionId = 1;
  string teamId = 2;
  string fileId = 3;
  string parentFolderId = 4;
  BasicFileInfo basicInfo = 5;
}

message CreateFileResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_PARENT_FOLDER = 4;
    CODE_NO_FILE = 5;
  }
  CODE code = 1;
  string errMsg = 2;
}

message ListFileRequest {
  string sessionId = 1;
  string teamId = 2;
  string parentFolderId = 3;
}

message ListFileResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_PARENT_FOLDER = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  repeated FileInfo fileList = 3;
}

message UpdateFileBasicRequest {
  string sessionId = 1;
  string teamId = 2;
  string fileId = 3;
  BasicFileInfo basicInfo = 4;
}

message UpdateFileBasicResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FILE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message UpdateFilePermRequest {
  string sessionId = 1;
  string teamId = 2;
  string fileId = 3;
  Perm perm = 4;
}

message UpdateFilePermResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FILE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
}

message GetFileRequest {
  string sessionId = 1;
  string teamId = 2;
  string fileId = 3;
}

message GetFileResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FILE = 4;
  }
  CODE code = 1;
  string errMsg = 2;
  FileInfo file = 3;
}

message RemoveFileRequest {
  string sessionId = 1;
  string teamId = 2;
  string fileId = 3;
}

message RemoveFileResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
  }
  CODE code = 1;
  string errMsg = 2;
}

message MoveFileRequest {
  string sessionId = 1;
  string teamId = 2;
  string fileId = 3;
  string parentFolderId = 4;
}

message MoveFileResponse {
  enum CODE {
    CODE_OK = 0;
    CODE_WRONG_SESSION = 1;
    CODE_NO_TEAM = 2;
    CODE_NO_PERMISSION = 3;
    CODE_NO_FILE = 4;
    CODE_NO_PARENT_FOLDER = 5;
  }
  CODE code = 1;
  string errMsg = 2;
}